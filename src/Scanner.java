import java.util.*;

public class Scanner {

    // Definition der Token
    final static int OPEN = 1000;
    final static int CLOSE = 1001;
    final static int INTEGER = 1002;
    final static int REAL = 1003;
    final static int TERM_CHAR_HIT = 9999;


    // error while scanning
    final static int ERROR = 2000;

    // No fitting diagram
    // following: ERROR_STATE
    final static int ERROR_STATE = -1;

    // input buffer
    private char[] buffer;
    private int buffer_len;

    // pointer positions
    private int pos = 0;
    private int start_pos = 0;

    private LinkedList<Integer> tokens;

    public Scanner(String strBuffer) {
        this.buffer = (strBuffer + "#").toCharArray();
        this.buffer_len = this.buffer.length;
        tokens = new LinkedList<>();
    }

    public List<Integer> getTokenlist() {
        int token = 0;
        boolean error_hit = false;
        while (!bufferCompletelyRead() && !error_hit) {
            token = this.gettoken();
            if (token != TERM_CHAR_HIT) {
                tokens.add(token);
            }
            if (token == ERROR) {
                error_hit = true;
            }
        }
        return tokens;
    }

    // Returns the position of the next char to read
    public int getpos() {
        return pos;
    }

    private boolean bufferCompletelyRead() {
        return !(pos < buffer_len - 1);
    }

    private char nextchar() {
        char nextc = this.buffer[this.pos];
        this.pos += 1;

        return nextc;
    }

    private void stepback() {
        if (pos > 0) {
            this.pos -= 1;
        }
    }

    private boolean isdigit(char c) {
        return (c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c == '7' || c == '8' || c == '9');
    }

    private boolean isdelim(char c) {
        return (c == ' ' || c == '\t' || c == '\n');
    }

    private boolean issign(char c) {
        return (c == '+' || c == '-');
    }

    private int gettoken() {
        while (true) {
            this.consumeDelimiters();
            if (this.bufferCompletelyRead()) return TERM_CHAR_HIT;
            if (this.isOpen()) return OPEN;
            if (this.isClose()) return CLOSE;
            //if (this.isReal()) return REAL;
            if (this.isInteger()) return INTEGER;
            return ERROR;
        }
    }

    private boolean isOpen() {
        char c;
        int state = 0;

        while (true) {
            switch (state) {
                case 0:
                    c = this.nextchar();
                    if (c == '(') {
                        state = 1;
                    } else {
                        this.pos = this.start_pos;
                        return false;
                    }
                    break;
                case 1:
                    this.start_pos = this.pos;
                    return true;
                default:
                    System.out.println("Hit undefinded state in isOpen");
                    return false;
            }
        }
    }

    private boolean isClose() {
        char c;
        int state = 2;

        while (true) {
            switch (state) {
                case 2:
                    c = this.nextchar();
                    if (c == ')') {
                        state = 3;
                    } else {
                        this.pos = this.start_pos;
                        return false;
                    }
                    break;
                case 3:
                    this.start_pos = this.pos;
                    return true;
                default:
                    System.out.println("Hit undefinded state in isClose");
                    return false;
            }
        }
    }

    private boolean isInteger() {
        char c;
        int state = 7;

        while (true) {
            switch (state) {
                case 7:
                    c = this.nextchar();
                    if (this.isdigit(c)) {
                        state = 8;
                    } else if (this.issign(c)) {
                        state = 9;
                    } else {
                        this.pos = this.start_pos;
                        return false;
                    }
                    break;
                case 8:
                    c = this.nextchar();
                    if (this.isdigit(c)) {
                        state = 8;
                    } else {
                        state = 10;
                    }
                    break;
                case 9:
                    c = this.nextchar();
                    if (this.isdigit(c)) {
                        state = 8;
                    } else {
                        this.pos = this.start_pos;
                        return false;
                    }
                    break;
                case 10:
                    this.stepback();
                    this.start_pos = this.pos;
                    return true;
                default:
                    System.out.println("Hit undefinded state in isInteger");
                    return false;
            }
        }
    }

    private boolean isReal() {
        char c;
        int state = 7;  // because let's check if it's an integer first

        // Hack: we have to save and restore the pos,
        // because isInteger moves it!
        int stored_pos = start_pos;

        if (this.isInteger()) {
            c = this.nextchar();
            if(c == '.') {
                state = 14;
            } else {
                this.pos = start_pos;
                return false;
            }

        } else {
            this.pos = start_pos;
            return false;
        }

        while (true) {
            switch (state) {
                case 14:

                    if (this.isdigit(c)) {
                        state = 15;
                    } else if (c == 'E') {
                        state = 17;
                    } else {
                        state = 16;
                    }
                    break;
                case 15:
                    c = this.nextchar();
                    if (this.isdigit(c)) {
                        state = 15;
                    } else if (c == 'E') {
                        state = 17;
                    } else {
                        state = 16;
                    }
                    break;
                case 16:
                    this.stepback();
                    this.start_pos = this.pos;
                    return true;
                case 17:
                    c = this.nextchar();
                    if (this.isdigit(c)) {
                        state = 18;
                    } else if (this.issign(c)) {
                        state = 19;
                    } else {
                        this.pos = this.start_pos;
                        return false;
                    }
                    break;
                case 18:
                    c = this.nextchar();
                    if (this.isdigit(c)) {
                        state = 18;
                    } else {
                        state = 16;
                    }
                    break;
                case 19:
                    c = this.nextchar();
                    if (this.isdigit(c)) {
                        state = 18;
                    } else {
                        this.pos = this.start_pos;
                        return false;
                    }
                    break;
                default:
                    System.out.println("Hit undefinded state in isReal");
                    return false;
            }
        }
    }

    private void consumeDelimiters() {
        char c;
        int state = 4;

        while (true) {
            switch (state) {
                case 4:
                    c = this.nextchar();
                    if (isdelim(c)) {
                        state = 5;
                    } else {
                        this.pos = this.start_pos;
                        return;
                    }
                    break;
                case 5:
                    c = this.nextchar();
                    if (isdelim(c)) {
                        state = 5;
                    } else {
                        state = 6;
                    }
                    break;
                case 6:
                    this.stepback();
                    this.start_pos = this.pos;
                    return;
            }
        }
    }
}
